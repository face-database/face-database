import hashlib
import os
import sqlite3
from typing import List, Dict, Optional, Tuple, Callable

import face_recognition
import numpy as np
from PIL import Image


class FaceDatabase:
    def __init__(self, db_path: str = "face_encodings.db"):
        self.db_path = db_path
        self.setup_database()

    @staticmethod
    def file_hash(filepath: str) -> str:
        """Return MD5 hash of a file."""
        with open(filepath, 'rb') as f:
            return hashlib.md5(f.read()).hexdigest()

    def setup_database(self) -> None:
        """Create necessary tables in the database."""
        with sqlite3.connect(self.db_path) as conn:
            cursor = conn.cursor()
            cursor.execute("""
            CREATE TABLE IF NOT EXISTS face_encodings (
                id INTEGER PRIMARY KEY,
                filename TEXT,
                filehash TEXT UNIQUE,
                encoding BLOB
            )
            """)
            cursor.execute("""
            CREATE TABLE IF NOT EXISTS settings (
                key TEXT PRIMARY KEY,
                value TEXT
            )
            """)

    def index_files(self, folder_path: str, progress_callback: Optional[Callable[[int, int], None]] = None) -> None:
        """
        Index all images in a folder.
        The progress_callback function, if provided, is called with the current progress and total files.
        """
        with sqlite3.connect(self.db_path) as conn:
            cursor = conn.cursor()
            all_files = [os.path.join(dp, f) for dp, dn, filenames in os.walk(folder_path) for f in filenames]

            for i, image_path in enumerate(all_files):
                if not self.is_valid_image(image_path):
                    continue

                current_file_hash = self.file_hash(image_path)
                cursor.execute("SELECT * FROM face_encodings WHERE filehash=?", (current_file_hash,))
                if cursor.fetchone():
                    continue

                image = face_recognition.load_image_file(image_path)
                face_encodings = face_recognition.face_encodings(image)

                for face_encoding in face_encodings:
                    cursor.execute(
                        "INSERT OR IGNORE INTO face_encodings (filename, filehash, encoding) VALUES (?, ?, ?)",
                        (image_path, current_file_hash, face_encoding.tobytes())
                    )

                if progress_callback:
                    progress_callback(i + 1, len(all_files))

            cursor.execute("INSERT OR REPLACE INTO settings (key, value) VALUES (?, ?)",
                           ("last_indexed_folder", str(folder_path)))

    def get_last_indexed_folder(self) -> Optional[str]:
        """Return the last indexed folder."""
        with sqlite3.connect(self.db_path) as conn:
            cursor = conn.cursor()
            cursor.execute("SELECT value FROM settings WHERE key=?", ("last_indexed_folder",))
            folder = cursor.fetchone()
            return folder[0] if folder else None

    def search_files(self, base_path: List[str], progress_callback: Optional[Callable[[int, int, int], None]] = None) -> \
            Dict[str, List[str]]:
        """
        Search for matching images based on the provided base_path.
        The progress_callback function, if provided, is called with the current progress and total files.
        """
        matches = {}
        if not base_path:
            return matches
        if len(base_path) == 1 and base_path[0] == "":
            return matches
        all_files = base_path

        total_files = len(all_files)
        total_faces = 0

        with sqlite3.connect(self.db_path) as conn:
            cursor = conn.cursor()
            cursor.execute("SELECT filename, encoding FROM face_encodings")
            known_encodings = {row[0]: np.frombuffer(row[1], dtype=np.float64) for row in cursor.fetchall()}

            for i, file_path in enumerate(all_files):
                try:
                    image = face_recognition.load_image_file(file_path)
                    face_encodings = face_recognition.face_encodings(image)
                    total_faces += len(face_encodings)

                    for face_encoding in face_encodings:
                        for known_file, known_encoding in known_encodings.items():
                            if face_recognition.compare_faces([known_encoding], face_encoding, tolerance=0.5)[0]:
                                matches.setdefault(file_path, []).append(known_file)
                except Exception as error:
                    print(error)

                if progress_callback:
                    # Учет и текущего файла, и количества обработанных лиц
                    progress_value = (i + 1) + total_faces
                    total_progress = total_files + total_faces
                    progress_callback(progress_value, total_progress, i)
        return matches

    def is_valid_image(self, file_path: str) -> bool:
        """
        Check if the file is a valid image and is not an EPS format.
        """
        try:
            with Image.open(file_path) as im:
                im.verify()
                return im.format != "EPS"
        except Exception:
            return False

    def get_num_files_indexed(self) -> int:
        """Return the total number of records."""
        with sqlite3.connect(self.db_path) as conn:
            cursor = conn.cursor()
            cursor.execute("SELECT COUNT(*) FROM face_encodings")
            return cursor.fetchone()[0]

    def get_last_added_file(self) -> Optional[str]:
        """Return the filename of the last added file."""
        with sqlite3.connect(self.db_path) as conn:
            cursor = conn.cursor()
            cursor.execute("SELECT filename FROM face_encodings ORDER BY id DESC LIMIT 1")
            file = cursor.fetchone()
            return file[0] if file else None
