import hashlib
import os
import unittest
from pathlib import Path

from face_database import FaceDatabase


class TestFaceDatabase(unittest.TestCase):
    test_dir: Path

    def setUp(self):
        self.test_dir = Path(os.curdir).joinpath("tests")
        self.db_path = Path(os.curdir).joinpath("test.db")
        self.face_db = FaceDatabase(db_path=self.db_path)

    def test_file_hash(self):
        file_path = os.path.join(self.test_dir, "test.txt")
        with open(file_path, 'w') as f:
            f.write("test content")
        self.assertEqual(self.face_db.file_hash(file_path), hashlib.md5(b"test content").hexdigest())

    def test_setup_database(self):
        self.assertTrue(os.path.exists(self.db_path))

    def test_index_and_search_files(self):
        # Пути к тестовым изображениям
        obama_image1 = self.test_dir.joinpath("obama.jpeg")
        obama_image2 = self.test_dir.joinpath("obama_good.jpg")
        trump_image = self.test_dir.joinpath("trump.jpg")

        # Индексация первого изображения Обамы
        self.face_db.index_files(self.test_dir)

        # Поиск Обамы на втором изображении
        matches_with_obama = self.face_db.search_files([obama_image1])

        # Поиск Обамы на изображении Трампа
        matches_with_trump = self.face_db.search_files([trump_image])

        # Проверяем, что результат поиска содержит первое изображение Обамы
        self.assertTrue(str(obama_image2) in [
            image for image in (matches_with_obama.get(obama_image1) or [])
        ])

        # Проверяем, что Обамы нет на изображении Трампа
        self.assertFalse(str(obama_image2) in [
            image for image in (matches_with_trump.get(trump_image) or [])
        ])


if __name__ == "__main__":
    unittest.main()