# word_report_generator.py
from pathlib import Path

from docx import Document
from docx.shared import Inches
from datetime import datetime
import os


class ReportGenerator:

    @staticmethod
    def generate(results: list, save_dir: str = None) -> str:
        """
        Генерирует отчет в формате .docx на основе предоставленных результатов поиска.

        :param results: Список кортежей, где первый элемент - искомый файл, второй - найденный файл.
        :param save_dir: Директория, куда будет сохранен отчет. Если None, отчет сохраняется рядом с search_path.
        :return: Путь к сгенерированному отчету.
        """

        doc = Document()
        doc.add_heading('Отчет о поиске', level=1)

        doc.add_paragraph(f'Дата поиска: {datetime.now().strftime("%d-%m-%Y %H:%M:%S")}')

        doc.add_heading('Результаты поиска', level=2)

        for original, match in results:
            table = doc.add_table(rows=2, cols=2)
            table.style = 'Table Grid'

            orig_cell, match_cell = table.rows[0].cells
            orig_cell.text = f'Искомый файл: {original}'
            match_cell.text = f'Найденный файл: {match}'

            orig_image_cell, match_image_cell = table.rows[1].cells
            if os.path.exists(original):
                orig_image_cell.paragraphs[0].add_run().add_picture(original, width=Inches(2))
            if os.path.exists(match):
                match_image_cell.paragraphs[0].add_run().add_picture(match, width=Inches(2))

            doc.add_paragraph()

        save_dir = Path(save_dir) if save_dir else Path(os.curdir)
        formatted_date = datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
        save_path = save_dir.joinpath(f"search_report_{formatted_date}.docx")
        doc.save(save_path)

        return str(save_path.absolute())


# Пример использования:
if __name__ == "__main__":
    results = [
        ('tests_b/obama.jpeg', 'tests_b/obama.jpeg'),
        ('tests_b/obama_good.jpg', 'tests_b/obama_good.jpg'),
        ('tests_b/trump.jpg', 'tests_b/trump.jpg'),
    ]
    report_path = ReportGenerator.generate(results)
    print(f"Отчет сохранен по пути: {report_path}")
