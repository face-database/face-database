import os
import platform
import subprocess
import threading
import tkinter as tk
from tkinter import ttk, filedialog, messagebox
from typing import List, Dict, Any

from ttkthemes import ThemedTk

from face_database.face_database import FaceDatabase
from report_generators.word_report_generator import ReportGenerator


class FaceDatabaseUI:
    def __init__(self):
        self.report_generator: ReportGenerator = ReportGenerator()
        self.face_db: FaceDatabase = FaceDatabase()
        self.window: ThemedTk = ThemedTk(theme="arc")
        self.window.title("Интерфейс базы данных лиц")
        self.create_widgets()

    def create_widgets(self) -> None:
        """Создание основных виджетов и вкладок."""
        self.notebook: ttk.Notebook = ttk.Notebook(self.window)

        # Создание вкладки индексации
        self.init_indexing_tab()

        # Создание вкладки поиска
        self.init_search_tab()

        self.notebook.pack(expand=True, fill='both')

    def init_indexing_tab(self) -> None:
        """Инициализация вкладки индексации."""
        self.tab1: ttk.Frame = ttk.Frame(self.notebook, padding="20")
        self.notebook.add(self.tab1, text='Индексация')

        # Виджеты выбора папки
        self.folder_label: ttk.Label = ttk.Label(self.tab1, text="Выберите папку для индексации:", font=('Arial', 12))
        self.folder_label.grid(row=0, column=0, columnspan=2, sticky=tk.W, pady=(0, 10))

        self.folder_path: tk.StringVar = tk.StringVar()
        self.folder_entry: ttk.Entry = ttk.Entry(self.tab1, textvariable=self.folder_path, width=50)
        self.folder_entry.grid(row=1, column=0, padx=(0, 10), pady=10, sticky=tk.EW)

        self.browse_button: ttk.Button = ttk.Button(self.tab1, text="Обзор", command=self.browse_folder)
        self.browse_button.grid(row=1, column=1, padx=(10, 0), pady=10, sticky=tk.W)

        # Кнопка индексации
        self.index_button: ttk.Button = ttk.Button(self.tab1, text="Начать индексацию", command=self.start_indexing,)
        self.index_button.grid(row=2, column=0, columnspan=2, pady=(10, 20), sticky=tk.EW)

        # Прогресс-бар для индексации
        self.progress: ttk.Progressbar = ttk.Progressbar(self.tab1, orient=tk.HORIZONTAL, length=400,
                                                         mode='determinate')
        self.progress.grid(row=3, column=0, columnspan=2, pady=(0, 20), sticky=tk.EW)

        # Отображение статистики базы данных
        self.stats_label: ttk.Label = ttk.Label(self.tab1,
                                                text="Статистика базы данных: 0 файлов проиндексировано.\nПоследний добавленный: Нет",
                                                font=('Arial', 10))
        self.stats_label.grid(row=4, column=0, columnspan=2, sticky=tk.W, pady=10)

        # Растягивание колонок и строк
        self.tab1.columnconfigure(0, weight=1)
        self.tab1.rowconfigure(4, weight=1)

    def init_search_tab(self) -> None:
        """Инициализация вкладки поиска."""
        self.tab2: ttk.Frame = ttk.Frame(self.notebook, padding="10")
        self.notebook.add(self.tab2, text='Поиск')

        # Элементы управления для выбора файла/папки
        self.init_search_controls()

    def init_search_controls(self) -> None:
        """Элементы управления для выбора файлов или папок для поиска."""
        self.search_label: ttk.Label = ttk.Label(self.tab2, text="Выберите изображения или папку для поиска:")
        self.search_label.grid(row=0, column=0, padx=20, pady=10, sticky=tk.W, columnspan=2)

        self.search_path: tk.StringVar = tk.StringVar()
        self.search_entry: ttk.Entry = ttk.Entry(self.tab2, textvariable=self.search_path, width=50)
        self.search_entry.grid(row=1, column=0, padx=20, pady=5, sticky=tk.EW, columnspan=2)

        self.search_file_browse_button: ttk.Button = ttk.Button(self.tab2, text="Выберите файл",
                                                                command=self.browse_file_search)
        self.search_file_browse_button.grid(row=2, column=0, padx=(20, 10), pady=5, sticky=tk.W)

        self.search_folder_browse_button: ttk.Button = ttk.Button(self.tab2, text="Выберите папку",
                                                                  command=self.browse_folder_search)
        self.search_folder_browse_button.grid(row=2, column=1, padx=(10, 20), pady=5, sticky=tk.E)

        self.search_button: ttk.Button = ttk.Button(self.tab2, text="Искать", command=self.start_searching)
        self.search_button.grid(row=3, column=0, padx=(20, 10), pady=5, sticky=tk.W)

        # Элементы управления для выбора пути сохранения отчета
        self.save_label: ttk.Label = ttk.Label(self.tab2, text="Выберите путь для сохранения отчета:")
        self.save_label.grid(row=4, column=0, padx=20, pady=10, sticky=tk.W, columnspan=2)

        self.save_path: tk.StringVar = tk.StringVar()
        self.save_entry: ttk.Entry = ttk.Entry(self.tab2, textvariable=self.save_path, width=50)
        self.save_entry.grid(row=5, column=0, padx=20, pady=5, sticky=tk.W)

        self.save_browse_button: ttk.Button = ttk.Button(self.tab2, text="Выберите путь", command=self.browse_save_path)
        self.save_browse_button.grid(row=5, column=1, padx=(10, 20), pady=5, sticky=tk.W)

        # Результаты поиска
        self.results_label: ttk.Label = ttk.Label(self.tab2, text="Результаты:")
        self.results_label.grid(row=6, column=0, padx=20, pady=(10, 5), sticky=tk.W, columnspan=3)  # columnspan=3

        self.results_tree: ttk.Treeview = ttk.Treeview(self.tab2, columns=('Original', 'Match'), show='headings')
        self.results_tree.heading('Original', text='Оригинальный файл')
        self.results_tree.heading('Match', text='Найденное соответствие')
        self.results_tree.grid(row=7, column=0, padx=20, pady=5, sticky=tk.EW, columnspan=2)  # columnspan=2

        self.clear_button = ttk.Button(self.tab2, text="Очистить таблицу", command=self.clear_table)
        self.clear_button.grid(row=8, column=0, padx=20, pady=(5, 20), sticky=tk.W, columnspan=3)  # columnspan=3

        # Полоса прокрутки для таблицы
        self.tree_scrollbar: ttk.Scrollbar = ttk.Scrollbar(self.tab2, orient='vertical',
                                                           command=self.results_tree.yview)
        self.results_tree.configure(yscrollcommand=self.tree_scrollbar.set)
        self.tree_scrollbar.grid(row=7, column=2, padx=(0, 20), pady=5, sticky=tk.NS)  # column=2

        self.results_tree.bind("<Double-1>", self.open_selected_file)

        # Растягивание колонок и строк
        self.tab2.columnconfigure(0, weight=1)
        self.tab2.columnconfigure(1, weight=1)  # Дополнительное растягивание для колонки под полосой прокрутки
        for i in range(9):  # Делаем все строки растягивающимися
            self.tab2.rowconfigure(i, weight=1)

    def show_progress(self):
        self.progress_window = tk.Toplevel(self.tab2)
        self.progress_window.title("Прогресс")

        self.progress_label = ttk.Label(self.progress_window, text="Выполняется поиск...")
        self.progress_label.pack(padx=10, pady=5)

        self.progress_bar = ttk.Progressbar(self.progress_window, orient=tk.HORIZONTAL, length=300, mode='determinate')
        self.progress_bar.pack(padx=10, pady=5)

        self.progress_details = ttk.Label(self.progress_window, text="")
        self.progress_details.pack(padx=10, pady=5)

    def browse_save_path(self):
        """Функция для выбора пути сохранения."""
        directory = filedialog.askdirectory()
        if directory:
            self.save_path.set(directory)

    def browse_folder(self) -> None:
        """Opens a dialog to select a folder."""
        folder_selected = filedialog.askdirectory()
        if folder_selected:
            self.folder_path.set(folder_selected)

    def start_indexing(self) -> None:
        """Starts the indexing process."""
        folder = self.folder_path.get()
        if not folder:
            messagebox.showerror("Error", "Please select a folder.")
            return
        threading.Thread(target=self.run_indexing, args=(folder,)).start()

    def run_indexing(self, folder: str) -> None:
        """Run the indexing process on the given folder."""

        def progress_callback(current: int, total: int) -> None:
            """Updates the progress bar with the current progress."""
            self.progress["value"] = (current / total) * 100
            self.window.update_idletasks()

        self.face_db.index_files(folder, progress_callback)
        self.window.after(0, lambda: self.update_after_indexing(folder))

    def update_after_indexing(self, folder: str) -> None:
        """Updates the UI after indexing is completed."""
        messagebox.showinfo("Success", "Indexing completed.")
        self.refresh_database_stats()

    def start_searching(self) -> None:
        """Starts the search process."""
        files: List[str] = self.search_path.get().split(', ')
        threading.Thread(target=self.run_searching, args=(files,)).start()

    def run_searching(self, files: List[str]) -> None:
        """Run the search process on the given files."""

        if len(files) == 1 and files[0] == "":
            return
        if files:
            self.show_progress()

        def search_progress_callback(current: int, total: int, total_files: int) -> None:
            """Updates the search progress bar with the current progress."""
            self.progress_bar["value"] = (current / total) * 100
            self.progress_details['text'] = f"Обработано {current} из {total} файлов"
            self.window.update_idletasks()

        results = self.face_db.search_files(files, search_progress_callback)
        if results:
            report_result = []
            for original, matches in results.items():
                for match in matches:
                    report_result.append((original, match))
            report_path = self.report_generator.generate(report_result, self.save_path.get())
            # Если вы хотите показать пользователю сообщение о том, где был сохранен отчет:
            messagebox.showinfo("Отчет сохранен", f"Отчет сохранен по пути: {report_path}")
        self.window.after(0, lambda: self.update_after_searching(results))

    def update_after_searching(self, results: Dict[str, List[str]]) -> None:
        """Updates the UI with search results."""
        for original, matches in results.items():
            for match in matches:
                self.results_tree.insert("", 'end', values=(original, match))

    def clear_table(self) -> None:
        """Clears all entries from the results tree."""
        for item in self.results_tree.get_children():
            self.results_tree.delete(item)

    def refresh_database_stats(self) -> None:
        """Refreshes the displayed database stats."""
        num_files = self.face_db.get_num_files_indexed()
        last_added = self.face_db.get_last_added_file() or "None"
        self.stats_label.config(text=f"Database Stats: {num_files} files indexed.\nLast added: {last_added}")

    def browse_file_search(self) -> None:
        """Browse for a single file to search."""
        file_selected = filedialog.askopenfilename()
        if file_selected:
            self.search_path.set(file_selected)

    def browse_folder_search(self) -> None:
        """Browse for a folder to search within."""
        folder_selected = filedialog.askdirectory()
        if folder_selected:
            files = [folder_selected] if os.path.isfile(folder_selected) else [os.path.join(dp, f) for dp, dn, filenames
                                                                               in
                                                                               os.walk(folder_selected) for f in
                                                                               filenames if
                                                                               self.face_db.is_valid_image(
                                                                                   os.path.join(dp, f))]
            # files = [os.path.join(folder_selected, f) for f in os.listdir(folder_selected) if
            #          os.path.isfile(os.path.join(folder_selected, f))]
            self.search_path.set(', '.join(files))

    def open_selected_file(self, event: Any) -> None:
        """Opens the selected file from the results treeview based on clicked column."""
        item = self.results_tree.selection()[0]
        col = self.results_tree.identify_column(event.x)  # определение колонки, по которой произведен клик

        # Если был клик по колонке 'Original', то выбрать путь из этой колонки
        if col == '#1':
            file_path = self.results_tree.item(item, "values")[0]
        # Если был клик по колонке 'Match', то выбрать путь из этой колонки
        elif col == '#2':
            file_path = self.results_tree.item(item, "values")[1]
        else:
            return  # Если клик был не по одной из этих колонок, ничего не делать

        # Открытие выбранного файла
        if platform.system() == "Windows":
            os.startfile(file_path)
        elif platform.system() == "Darwin":
            subprocess.call(("open", file_path))
        else:
            subprocess.call(("xdg-open", file_path))

    def run(self) -> None:
        """Основной цикл событий для пользовательского интерфейса."""
        # Обновление статистики при запуске приложения
        total_files = self.face_db.get_num_files_indexed()
        last_file = self.face_db.get_last_added_file() or "Нет"  # используйте "Нет", если файлов нет
        self.stats_label.config(
            text=f"Статистика базы данных: {total_files} файлов проиндексировано.\nПоследний добавленный: {last_file}")
        self.window.mainloop()